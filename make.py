#!/usr/bin/python

import subprocess, urllib2, sys

url = 'https://github.com/elliottslaughter/notebook2011/raw/master/notebook.in'
filename_in = 'notebook.in'
filename_tex = 'notebook2011.tex'

if __name__ == '__main__':
    #filein = urllib2.urlopen(url)
    #fileout = open(filename_in, 'w')
    #fileout.write(filein.read())
    #fileout.close()
    #filein.close()
    fileout = open(filename_tex, 'w')
    retcode = subprocess.call(['python', 'gen.py'], stdout = fileout)
    fileout.close()
    if retcode != 0:
        sys.exit(1)
    retcode = subprocess.call(['pdflatex', filename_tex])
    if retcode != 0:
        sys.exit(1)
    retcode = subprocess.call(['pdflatex', filename_tex])
    if retcode != 0:
        sys.exit(1)
