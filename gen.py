#!/usr/bin/python

import sys, re, string

school = r'University of California---San Diego'
author = r'Haoxi Fang, David Michon, and Elliott Slaughter'
header = r'''
\documentclass[letterpaper]{article}
\usepackage{amsmath}
\usepackage[margin=1in]{geometry}
\pagestyle{myheadings}
\author{%(author)s}
\title{%(school)s}

\begin{document}
\maketitle
\tableofcontents

\pagebreak
''' % {'school': school, 'author': author}
footer = r'''
\end{document}
'''

def checksum (ss):
    a = ''
    o = []
    for s in ss.split('\n'):
        if s.rfind('//----')>=0: a = ''
        a += re.sub('//.*','',s).strip()
        o.append(('%04x: %s' % (hash(a) & 0xffff, s)).strip())
    return '\n'.join(o)

def format_section (header):
    if header[0] == '%':
        return '\section{%s}' % header[1:].strip()
    else:
        return '\subsection{%s}' % header.strip()

def format_verbatim (body):
    if len(body.strip()) == 0:
        return ''
    return r'''\begin{verbatim}
%s
\end{verbatim}''' % checksum(string.replace(body, '\t', '    ').strip())

re_section = re.compile(r'^[%]', re.MULTILINE)
re_section_header = re.compile(r'[:]?$', re.MULTILINE)
def split_sections (s):
    sections = re.split(re_section, s)
    sections = filter(lambda x: len(x.strip()) > 0, sections)
    return [re.split(re_section_header, section, maxsplit = 1) for section in sections]

if __name__ == '__main__':
    print header
    for (sheader, sbody) in split_sections(open("notebook.in").read()):
        print format_section(sheader)
        print format_verbatim(sbody)
    print footer
